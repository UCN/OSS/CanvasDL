module.exports = {
  // Replace with your Canvas installation URL (usually provided by your University)
  URL: `https://ucn.instructure.com/api/v1/`,

  // Generate a token on your "/profile/settings/" page
  token: `4718~ZoynPUS6PwO8TeJQIzNC2eL9JyaPfmkyYx0JCSPuBTtffSiT4WozuRNhbzcQnq9M`,
  // Note: You MUST generate your own token. The above is just for display purposes and it's expired. It will NOT work.

  // All the courses for which you want to download everything in the "Modules" submenu
  // Navigate to your desired course and copy the ID from the URL bar and the name from the page
  coursesWithModules: function () {
    return {
      10957: `Databases (psu0217)`,
      10959: `Design by Contr (psu0217)`,
      10963: `Test (psu0217)`,
      11194: `Web Dev. 2 (Web Dev.)`,
    };
  },

  // All the courses for which you want to download everything in the "Files" submenu
  // Navigate to your desired course and copy the ID from the URL bar and the name from the page
  coursesWithFiles: function () {
    return {
      10957: `Databases (psu0217)`,
      10959: `Design by Contr (psu0217)`,
      11221: `General info (psu0217)`,
      10961: `Project 1 (psu0217)`,
      10963: `Test (psu0217)`,
      11194: `Web Dev. 2 (Web Dev.)`,
    };
  },
}

