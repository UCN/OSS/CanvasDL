# CanvasDL

## A simple and poorly coded Node.js script for downloading resources from the Canvas LMS.

### Installation and Usage
1. Make sure you have the latest version of Node.js installed. If not, head over to [nodejs.org](https://nodejs.org/en/)
2. Open a terminal and run `git clone git@gitlab.com:UCN/CanvasDL.git`. If you don't have Git installed, download the latest release of **CanvasDL** from [here](https://gitlab.com/UCN/CanvasDL/repository/archive.zip?ref=master) and unzip the file to some folder. Should look like this:
![](img/c34e8014e7e84ba3643e5d015531628c.png)
3. Open a terminal inside the **CanvasDL** folder. Watch this amazing GIF if you are an inexperienced Windows user:
![](img/80dad481d2eb948789be9d9926679f6a.gif)
4. Run `npm install` and wait for all dependencies to download. Should look like this at the end:
![](img/3210349577a9c9f2e44b6f84720b0b56.png)
5. Open `config.js` in a text editor:
![](img/05f35ae65c7b65ea8adc4e60a348b8ed.png)
6. Modify the `URL` to match the one you use to login at your University. If you are an UCN student, it's already done for you.
7. Go to your Canvas Profile Settings page, scroll to the bottom, and press **+ New Access Token**. Enter some purpose there and then press **Generate Token**:
![](img/d8152ec97adde6be235fc9c18a47b312.png)
8. Replace the `token` inside the `config.js` file with the one you just received:
![](img/e9e54b9a7467f35839eb61f32dccce7d.png)
9. Navigate through your Courses and pick the ones you are interested to download. If they have a **Modules** submenu, you can add them to the first section (`coursesWithModules`). If they have a **Files** submenu, you can add them to the second section (`coursesWithFiles`). You get the course ID from the URL bar, and the title from the page. If one course appears in both sections, make sure it has the exact same name in both places:
![](img/783785ada07d36a3457f0b7008b82944.png)
10. Repeat the above step for all your courses that have either a **Files** submenu, a **Modules** submenu, or both. Make sure you remove anything that you didn't enter (the example courses from the above picture).
11. Save the `config.js` file, close your text editor, switch back to your terminal.
12. Type `node index`. If you did everything as described in the above steps, the script should start downloading all the files for the courses you have selected. Wait until it's done. You should see a new folder named `dl`. All your files are there. You can close the terminal now.
![](img/e82e8ee6c4fd531882a723db5cf8ca79.png)

Enjoy the speed of having all your files on your local PC, and forget about the slow Canvas!

N00bTip™: Every day before starting classes, run the script once to sync your laptop with Canvas. It literally takes 3 seconds. Wait until it syncs and off you go. However, please note that at its current state, the script will overwrite everything. So if you made changes to any file, you will lose them.
![](img/ccd414ef96e4725b5d3cf261357e0a1f.gif)

ProTip™: Edit the `.gitignore` file to track the `dl` folder as well. This way you can have a history of all the changes ever made to the files on Cavas (like seeing if teachers made a mistake and then trying to fix it). But don't forget to have a separate copy of the repo in case you are planning to contribute.


### State of the project
It was coded in a couple of hours. No code standard and little testing. It just does the job and nothing more.

### Contribute
Feel free to open pull requests to improve the project.

You can work on:
- [ ] Commenting the code
- [ ] Optimizing the code
- [ ] Writing tests
- [ ] Adding extra functionality
- [ ] Adding more useful verbose to the CLI
- [ ] Fixing or submitting [issues](https://gitlab.com/UCN/OSS/CanvasDL/issues)
- [ ] Anything else

A complete list of API Resources can be found on the official [Canvas LMS REST API Documentation page](https://canvas.instructure.com/doc/api/all_resources.html).

Please note that you might not be able to access all the endpoints as your student account is certainly limited.

If you want to play with everything, head over to [Bitnami](https://bitnami.com/stack/canvaslms) and get your own private Canvas up and running in a matter of minutes.

#### Contributors
@futz.co

