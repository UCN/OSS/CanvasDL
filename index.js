// set to `true` for a little bit more verbose
const _DEBUG = false;

// load config
const _config = require(`./config.js`);
const _url = _config.URL;
const _apiToken = _config.token;
const _coursesWithModules = _config.coursesWithModules();
const _coursesWithFiles = _config.coursesWithFiles();

// import `_DEBUG` mode helpers
if (_DEBUG) {
  const util = require(`util`);
  require(`better-buffer-inspect`);
  require('request-debug')(request);
}

// save errors here for some reason
let _allErrors = ``;

// imports
const colors = require(`colors`);
const request = require(`request`);
const fs = require(`fs`);

// set color themes
colors.setTheme({
  _info: `cyan`,
  _success: `green`,
  _error: `red`,
  _retarded: `rainbow`,
});

// Download Files
Promise.all(Object.keys(_coursesWithFiles).map((courseId) => {
  return new Promise((resolve) => {
    let exportsURL = `${_url}courses/${courseId}/content_exports?access_token=${_apiToken}`;
    let courseName = _coursesWithFiles[courseId];
    downloadFilesForCourse(exportsURL, courseName, 0, resolve);
  });
})).then(() => {
  console.log(`Successfully finished downloading all the courses' files.`._success);
});


//////

var time = 1;
const timeIncrease = 500;
const timeDivisor = 2;

let errors = ``;


var urls = ``;

try {

  const Client = require(`node-rest-client`).Client;
  const client = new Client();

  const DL = require(`download-file`);

  const url = require(`url`);
  const followRedirects = require(`follow-redirects`);
  const http = followRedirects.http;
  const https = followRedirects.https;

  const extract = require(`extract-zip`);

  const mkdirp = require(`mkdirp`);
  const os = require(`os`);


  const args = {
    requestConfig: {
      timeout: 1000 * 60 * 5,
      noDelay: true,
      keepAlive: true,
      keepAliveDelay: 500,
      followRedirects: true,
      maxRedirects: 21,
    },
    responseConfig: {
      timeout: 1000 * 60 * 5,
    },
  };

  // Download Modules
  Object.keys(_coursesWithModules).forEach(courseId => {
    let modulesURL = `${_url}courses/${courseId}/modules?access_token=${_apiToken}`;
    let courseName = _coursesWithModules[courseId];
    console.log(`Downloading modules for "${courseName}" from\n${modulesURL}\n`);

    setTimeout(function () {
      client.get(modulesURL, args, function (data, response) {
        for (let i = 0; i < data.length; i++) {
          let moduleURL = `${data[i].items_url}?access_token=${_apiToken}`;
          let moduleName = data[i].name;
          console.log(`Entered ${moduleName} at\n${moduleURL}\n`);

          setTimeout(function () {
            client.get(moduleURL, args, function (data, response) {
              for (let j = 0; j < data.length; j++) {
                let fileURL = `${data[j].url}?access_token=${_apiToken}`;
                let fileName = data[j].title;
                console.log("fileURL:", fileURL);
                let directory = `./dl/${courseName}/Modules/${moduleName}/`;
                let dlOptions = {
                  directory: directory,
                  filename: fileName,
                };
                let currentPath = `${courseName} -> ${moduleName} -> ${fileName}`;
                if (data[j].type == `File`) {
                  setTimeout(function () {
                    client.get(fileURL, args, function (data, response) {
                      let downloadURL = data.url;
                      console.log(`${currentPath}:\n${downloadURL}\n`);
                      setTimeout(function () {
                        https.get(downloadURL, response => {
                          console.log(`response.responseUrl:`, response.responseUrl);
                          setTimeout(function () { // 1st try
                            DL(response.responseUrl, dlOptions, function (err) {
                              if (err) {
                                console.log(`Failed to download: ${currentPath} | Error: ${err}`);
                                setTimeout(function () { // 2nd try
                                  DL(response.responseUrl, dlOptions, function (err) {
                                    if (err) {
                                      console.log(`Failed to download: ${currentPath} | Error: ${err}`);
                                      setTimeout(function () { // 3rd try
                                        DL(response.responseUrl, dlOptions, function (err) {
                                          if (err) {
                                            console.log(`Failed to download: ${currentPath} | Error: ${err}`);
                                            errors += `Failed to download: ${currentPath} | Error: ${err}\n`;
                                          } else console.log(`Downloaded: ${currentPath}`);
                                        });
                                      }, 5000);
                                    } else console.log(`Downloaded: ${currentPath}`);
                                  });
                                }, 5000);
                              } else console.log(`Downloaded: ${currentPath}`);
                            });
                          }, time);
                          time += timeIncrease;
                        });
                      }, time);
                      time += timeIncrease;
                    });
                  }, time);
                  time += timeIncrease;
                } else if (data[j].type == `ExternalUrl`) { // Just create a shortcut in this case.
                  mkdirp(directory, function(e) {
                    if (e) {
                      console.log(e);
                      errors += e;
                    } else {
                      fs.writeFile(`${directory}/${fileName}.url`, `[InternetShortcut]${os.EOL}URL=${data[j].external_url}${os.EOL}`, function(err) {
                        if(err) {
                        console.log(`Failed to download: ${currentPath} | Error: ${err}`);
                        errors += `Failed to download: ${currentPath} | Error: ${err}\n`;
                        } else console.log(`Downloaded: ${currentPath}`);
                      });
                    }
                  });
                } else {
                  console.log(`Failed to download: ${currentPath} | Error: unknown type: ${data[j].type}`);
                  errors += `Failed to download: ${currentPath} | Error: unknown type: ${data[j].type}`;
                };
              };
            });
          }, time);
          time += timeIncrease;
        };
      });
    }, time);
    time += timeIncrease;
  });


  function unzipFile(source, target, cb) {
    extract(source, {
      dir: target
    }, function (err) {
      // console.log(`Failed to extract: ${source} | Error: ${err}`);
      cb();
    });
  };

  function downloadFile(URL, options, hrPath, cb) {
    setTimeout(function () {
      https.get(URL, response => {
        setTimeout(function () { // 1st try
          DL(response.responseUrl, options, function (err) {
            if (err) {
              console.log(`Failed to download: ${hrPath} | Error: ${err}`);
              setTimeout(function () { // 2nd try
                DL(response.responseUrl, options, function (err) {
                  if (err) {
                    console.log(`Failed to download: ${hrPath} | Error: ${err}`);
                    setTimeout(function () { // 3rd try
                      DL(response.responseUrl, options, function (err) {
                        if (err) {
                          console.log(`Failed to download: ${hrPath} | Error: ${err}`);
                          errors += `Failed to download: ${hrPath} | Error: ${err}\n`;
                        } else {
                          console.log(`Downloaded: ${hrPath}`);
                          cb();
                        };
                      });
                    }, 5000);
                  } else {
                    console.log(`Downloaded: ${hrPath}`);
                    cb();
                  };
                });
              }, 5000);
            } else {
              console.log(`Downloaded: ${hrPath}`);
              cb();
            };
          });
        }, time / timeDivisor);
        time += timeIncrease;
      });
    }, time / timeDivisor);
    time += timeIncrease;
  };
} catch (e) {
  errors += `Caught: ${e}`;
  console.log(e);
};

function downloadFilesForCourse(exportsURL, courseName, errorCount, cb) {
  console.log(`Generating export (trial #${errorCount + 1}) for "${courseName}" from\n${exportsURL}.\n`._info);
  request({
    method: `POST`,
    uri: `${exportsURL}&export_type=zip`,
    followRedirect: true,
    followAllRedirects: true,
    followOriginalHttpMethod: true,
    maxRedirects: 32,
    forever: true,
    timeout: 1000 * 60 * 5, // 5 minutes
    pool: { maxSockets: 1024 },
  }, function (error, response, body) {
    if (error) {
      errorCount += 1;
      let verbose = `Failed to generate the ZIP file export for: "${courseName}". | Trial: #${errorCount}. | Error: "${error}".\n`;
      _allErrors += verbose;
      console.log(verbose._error);
      setTimeout(() => { // take a break
        downloadFilesForCourse(exportsURL, courseName, errorCount, cb); // then try again
      }, 1000 * 10); // 10 seconds
    } else {
      body = JSON.parse(body);
      let progressURL = body[`progress_url`];
      let exportId = body[`id`];
      let completion = 0;
      let checkCompletion = setInterval(function () {
        request({
          method: `GET`,
          uri: `${progressURL}?access_token=${_apiToken}`,
        }, function (error, response, body) {
          body = JSON.parse(body);
          completion = body[`completion`];
        });
        if (completion == 100) {
          clearInterval(checkCompletion);
          request({
            method: `GET`,
            uri: exportsURL,
          }, function (error, response, body) {
            body = JSON.parse(body);
            for (let i = 0; i < body.length; i++) {
              if (body[i][`id`] == exportId) {
                let downloadURL = body[i][`attachment`][`url`];
                let downloadOptions = {
                  directory: `./dl/${courseName}/Files/`,
                  filename: body[i][`attachment`][`filename`],
                  timeout: 1000 * 60 * 5,
                };
                let hrPath = `${courseName} -> ${downloadOptions.filename}`;
                downloadFile(downloadURL, downloadOptions, hrPath, function () {
                  unzipFile(`${downloadOptions.directory}${downloadOptions.filename}`, downloadOptions.directory, function () {
                    fs.unlinkSync(`${downloadOptions.directory}${downloadOptions.filename}`);
                    console.log(`Successfully downloaded the course "${courseName}" on trial #${errorCount + 1}.\n`._success);
                    cb();
                  });
                });
              };
            };
          });
        };
      }, 1000 * 1); // 1 second
    };
  });
};
